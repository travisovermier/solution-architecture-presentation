﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Travis._383Spring2016Presentation.Core.Features.Projects;
using Travis._383Spring2016Presentation.Core.Features.Users;

namespace Travis._383Spring2016Presentation.Core.Data
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
            Configuration.AutoDetectChangesEnabled = true;
            Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations
                .Add(new UserConfiguration())
                .Add(new ProjectConfiguration());
        }
    }
}
