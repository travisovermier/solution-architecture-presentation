﻿using System.Web.Mvc;
using Travis._383Spring2016Presentation.Core.Features.Users;

namespace Travis._383Spring2016Presentation.Web.Controllers
{
    public class UserController : Controller
    {
        private IUserService userService;

        public ActionResult Index()
        {
            return Content(userService.GetCustomString());
        }

        public UserController(UserService userService)
        {
            this.userService = userService;
        }

        public ActionResult Details(long id)
        {
            return Json(userService.GetUserById(id));
        } 

        public ActionResult CreateUser(User user)
        {
            return Json(userService.CreateUser(user));
        }

        public ActionResult EditUser(User user)
        {
            return Json(userService.EditUser(user));
        }
    }
}