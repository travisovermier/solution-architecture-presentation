﻿namespace Travis._383Spring2016Presentation.Core.Features.Users
{
    public class User
    {
        public long Id { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }
    }
}