﻿using System.Data.Entity;
using MediatR;
using Travis._383Spring2016Presentation.Core.Data;

namespace Travis._383Spring2016Presentation.Core.Features.Projects
{
    public class GetProjectByIdQuery : IRequest<Project>
    {
        public int Id { get; set; }
    }

    public class GetProjectByIdQueryHandler : IRequestHandler<GetProjectByIdQuery, Project>
    {
        private DataContext dataContext;
        private readonly IDbSet<Project> Projects;

        public GetProjectByIdQueryHandler(DataContext dataContext)
        {
            this.dataContext = dataContext;
            Projects = dataContext.Set<Project>();
        }

        public Project Handle(GetProjectByIdQuery query)
        {
            return Projects.Find(query.Id);
        }
    }
}
