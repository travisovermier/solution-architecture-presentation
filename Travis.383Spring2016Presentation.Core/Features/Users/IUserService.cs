﻿namespace Travis._383Spring2016Presentation.Core.Features.Users
{
    public interface IUserService
    {
        User GetUserById(long id);
        long CreateUser(User user);
        User EditUser(User user);
        string GetCustomString();
    }
}
