﻿using MediatR;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Data.Entity;
using System.Reflection;
using System.Web.Mvc;
using Travis._383Spring2016Presentation.Core.Data;
using Travis._383Spring2016Presentation.Core.Features.Users;

namespace Travis._383Spring2016Presentation.Web.App_Start
{
    public class SimpleInjectorConfiguration
    {
        public static void Configure()
        {
            var lifestyle = new WebRequestLifestyle(true);
            var container = new Container();
            var entry = Assembly.GetEntryAssembly() ?? Assembly.GetCallingAssembly();
            foreach (var referencedAssembly in entry.GetReferencedAssemblies())
            {
                Assembly.Load(referencedAssembly);
            }

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.Register<DbContext>(() => new DataContext(), lifestyle);
            container.Register<IMediator, Mediator>();
            container.Register<IUserService, UserService>();

            // Shamelessly borrowed and edited from here https://github.com/jbogard/MediatR/blob/master/samples/MediatR.Examples.SimpleInjector/Program.cs
            container.Register(typeof(IRequestHandler<,>), AppDomain.CurrentDomain.GetAssemblies());
            container.Register(typeof(IAsyncRequestHandler<,>), AppDomain.CurrentDomain.GetAssemblies());
            container.RegisterCollection(typeof(INotificationHandler<>), AppDomain.CurrentDomain.GetAssemblies());
            container.RegisterCollection(typeof(IAsyncNotificationHandler<>), AppDomain.CurrentDomain.GetAssemblies());
            container.RegisterSingleton(new SingleInstanceFactory(container.GetInstance));
            container.RegisterSingleton(new MultiInstanceFactory(container.GetAllInstances));

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}