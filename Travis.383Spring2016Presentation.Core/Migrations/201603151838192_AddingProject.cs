namespace Travis._383Spring2016Presentation.Core.Data
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingProject : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Project",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        Client = c.String(nullable: false, maxLength: 255),
                        IsActive = c.Boolean(nullable: false),
                        UserId = c.Int(nullable: false),
                        User_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Project", "User_Id", "dbo.User");
            DropIndex("dbo.Project", new[] { "User_Id" });
            DropTable("dbo.Project");
        }
    }
}
