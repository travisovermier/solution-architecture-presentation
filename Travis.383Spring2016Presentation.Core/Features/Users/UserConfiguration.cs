﻿using System.Data.Entity.ModelConfiguration;

namespace Travis._383Spring2016Presentation.Core.Features.Users
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            Property(x => x.EmailAddress)
                .IsRequired()
                .HasMaxLength(255);

            Property(x => x.Password)
                .IsRequired();
        }
    }
}
