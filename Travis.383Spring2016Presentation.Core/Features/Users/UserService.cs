﻿using System.Data.Entity;
using System.Web.Helpers;
using Travis._383Spring2016Presentation.Core.Data;

namespace Travis._383Spring2016Presentation.Core.Features.Users
{
    public class UserService : IUserService
    {
        private DataContext dataContext;
        private IDbSet<User> Users;

        public UserService(DataContext dataContext)
        {
            this.dataContext = dataContext;
            Users = dataContext.Set<User>();
        }

        public User GetUserById(long id)
        {
            return dataContext.Set<User>().Find(id);
        }

        public long CreateUser(User user)
        {
            var password = Crypto.HashPassword(user.Password);
            user.Password = password;
            Users.Add(user);
            dataContext.SaveChanges();
            return user.Id;
        }

        public User EditUser(User user)
        {
            var oldUser = GetUserById(user.Id);
            oldUser.EmailAddress = user.EmailAddress;
            dataContext.SaveChanges();
            return oldUser;
        }

        public string GetCustomString()
        {
            return "Some string";
        }
    }
}
