﻿using System.Data.Entity.ModelConfiguration;

namespace Travis._383Spring2016Presentation.Core.Features.Projects
{
    public class ProjectConfiguration : EntityTypeConfiguration<Project>
    {
        public ProjectConfiguration()
        {
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(255);

            Property(x => x.Client)
                .IsRequired()
                .HasMaxLength(255);
        }
    }
}
