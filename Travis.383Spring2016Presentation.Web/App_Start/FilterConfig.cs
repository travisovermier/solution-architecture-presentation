﻿using System.Web;
using System.Web.Mvc;

namespace Travis._383Spring2016Presentation.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
