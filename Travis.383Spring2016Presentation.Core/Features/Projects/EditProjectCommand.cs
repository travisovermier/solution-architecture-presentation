﻿using MediatR;
using System.Data.Entity;
using Travis._383Spring2016Presentation.Core.Data;

namespace Travis._383Spring2016Presentation.Core.Features.Projects
{
    public class EditProjectCommand : IRequest<int>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Client { get; set; }

        public bool IsActive { get; set; }

        public int UserId { get; set; }
    }

    public class EditProjectCommandHandler : IRequestHandler<EditProjectCommand, int>
    {
        private DataContext dataContext;
        private readonly IDbSet<Project> Projects;

        public EditProjectCommandHandler(DataContext dataContext)
        {
            this.dataContext = dataContext;
            Projects = dataContext.Set<Project>();
        }

        public int Handle(EditProjectCommand command)
        {
            var project = Projects.Find(command.Id);

            project.Name = command.Name;
            project.Client = command.Client;
            project.IsActive = command.IsActive;
            project.UserId = command.UserId;

            dataContext.SaveChanges();
            return project.Id;
        }
    }
}
