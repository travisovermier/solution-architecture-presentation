﻿using System.Data.Entity;
using MediatR;
using Travis._383Spring2016Presentation.Core.Data;

namespace Travis._383Spring2016Presentation.Core.Features.Projects
{
    public class CreateProjectCommand : IRequest<int>
    {
        public string Name { get; set; }

        public string Client { get; set; }

        public bool IsActive { get; set; }

        public int UserId { get; set; }
    }

    public class CreateProjectCommandHandler : IRequestHandler<CreateProjectCommand, int>
    {
        private DataContext dataContext;
        private readonly IDbSet<Project> Projects;

        public CreateProjectCommandHandler(DataContext dataContext)
        {
            this.dataContext = dataContext;
            Projects = dataContext.Set<Project>();
        }

        public int Handle(CreateProjectCommand command)
        {
            var project = new Project
            {
                Name = command.Name,
                Client = command.Client,
                IsActive = command.IsActive,
                UserId = command.UserId
            };
            Projects.Add(project);

            dataContext.SaveChanges();
            return project.Id;
        }
    }
}
