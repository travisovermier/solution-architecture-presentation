﻿using Travis._383Spring2016Presentation.Core.Features.Users;

namespace Travis._383Spring2016Presentation.Core.Features.Projects
{
    public class Project
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Client { get; set; }

        public bool IsActive { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}
