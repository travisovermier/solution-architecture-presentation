﻿using System.Data.Entity.Migrations;
using Travis._383Spring2016Presentation.Core.Features.Projects;
using Travis._383Spring2016Presentation.Core.Features.Users;

namespace Travis._383Spring2016Presentation.Core.Data
{
    public class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataContext context)
        {
            SeedUsers(context);
            SeedProjects(context);
        }

        private void SeedUsers(DataContext context)
        {
            var users = context.Set<User>();
            users.AddOrUpdate(
                new User
                {
                    Id = 1,
                    EmailAddress = "tovermier@envoc.com",
                    Password = "password"
                },
                new User
                {
                    Id = 2,
                    EmailAddress = "tovermier1@envoc.com",
                    Password = "password"
                },
                new User
                {
                    Id = 3,
                    EmailAddress = "tovermier2@envoc.com",
                    Password = "password"
                },
                new User
                {
                    Id = 4,
                    EmailAddress = "tovermier3@envoc.com",
                    Password = "password"
                },
                new User
                {
                    Id = 5,
                    EmailAddress = "tovermier4@envoc.com",
                    Password = "password"
                });
        }

        private void SeedProjects(DataContext context)
        {
            var projects = context.Set<Project>();
            projects.AddOrUpdate(
                new Project
                {
                    Id = 1,
                    Name = "Project1",
                    Client = "Client1",
                    IsActive = true,
                    UserId = 1,
                },
                new Project
                {
                    Id = 2,
                    Name = "Project2",
                    Client = "Client2",
                    IsActive = true,
                    UserId = 2,
                },
                new Project
                {
                    Id = 3,
                    Name = "Project3",
                    Client = "Client3",
                    IsActive = true,
                    UserId = 3,
                },
                new Project
                {
                    Id = 4,
                    Name = "Project4",
                    Client = "Client4",
                    IsActive = true,
                    UserId = 4,
                });
        }
    }
}
