﻿using MediatR;
using System.Web.Mvc;
using Travis._383Spring2016Presentation.Core.Features.Projects;

namespace Travis._383Spring2016Presentation.Web.Controllers
{
    public class ProjectController : Controller
    {
        private IMediator Mediator;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Mediator = DependencyResolver.Current.GetService<IMediator>();
        }

        public ActionResult Index()
        {
            return Json(Mediator.Send(new GetTestStringQuery { QueryParameterString = "My param string" }), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int id)
        {
            return Json(Mediator.Send(new GetProjectByIdQuery { Id = id }), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateProject(CreateProjectCommand command)
        {
            return Json(Mediator.Send(command));
        }

        public ActionResult EditProject(EditProjectCommand command)
        {
            return Json(Mediator.Send(command));
        }
    }
}