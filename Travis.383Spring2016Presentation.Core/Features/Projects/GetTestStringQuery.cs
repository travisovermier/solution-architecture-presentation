﻿using MediatR;

namespace Travis._383Spring2016Presentation.Core.Features.Projects
{
    public class GetTestStringQuery : IRequest<string>
    {
        public string QueryParameterString { get; set; }
    }

    public class GetTestStringQueryHandler : IRequestHandler<GetTestStringQuery, string>
    {
        public string Handle(GetTestStringQuery message)
        {
            return "This is my test string plus my QueryParameterString: " + message.QueryParameterString;
        }
    }
}
